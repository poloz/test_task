import re
import numpy as proc
def pr():
	per = [50,75,95]
	iphone, mac, android, ipad, iphoneper, macper, androidper, ipadper = [],[],[],[],[],[],[],[]
	f = open('upload/logfile.log')
	line = f.readline()
	while line:
		line = f.readline()
		if "media_" in line:
			if "iPhone" in line:
				a = line.split('"')
				b = a[2].split(' ') 
				iphone.append(float(b[2]))

			if "Macintosh" in line:
				a = line.split('"')
				b = a[2].split(' ')
				mac.append(float(b[2]))

			if "Android" in line:
				a = line.split('"')
				b = a[2].split(' ')
				android.append(float(b[2]))

			if "iPad" in line:
				a = line.split('"')
				b = a[2].split(' ')
				ipad.append(float(b[2]))
	f.close()

	for k in per:
		try:
			iphoneper.append(round(proc.percentile(iphone, k),3))
		except: 
			iphoneper.append("no data for percentile " + str(k))
		try:
			macper.append(round(proc.percentile(mac, k),3))
		except:
			macper.append("no data forpercentile " + str(k))
		try:
			androidper.append(round(proc.percentile(android, k),3))
		except:
			androidper.append("no data for percentile " + str(k))
		try:
			ipadper.append(round(proc.percentile(ipad, k),3))	
		except:
			ipadper.append("no data for percentile" + str(k))
	return(androidper,iphoneper,ipadper,macper)

if __name__ == '__main__':
	pr()
