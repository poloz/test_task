FROM ubuntu:16.04
MAINTAINER vasiliy turuntaev 

WORKDIR /var/app

RUN apt-get update -y && \
    apt-get install -y python-pip python-dev

RUN pip install --no-cache-dir  flask ldap3 requests numpy

#COPY . /usr/src/app

# Expose the Flask port
EXPOSE 5000

CMD [ "python", "./app.py" ]
