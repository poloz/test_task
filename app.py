import zip
import parser
import os
import requests
from flask import render_template

from flask import Flask, request
app = Flask(__name__)


@app.route('/upload', methods=["GET", "POST"])
def upload():
	if request.method == 'POST':
		f = request.files['log']
		mimetype = f.content_type
		filename = f.filename.split('.')
		path = "/var/app/upload/logfile." + filename[-1]

		if mimetype != 'application/x-gzip' and mimetype != 'application/octet-stream':
			return("got a wrong mime "+mimetype)
		f.save(path)

		try:
			if filename[-1] == "gz": zip.unzip(path)
		except:
			return("error while unpacking archive")
	
		per = parser.pr()
		return render_template("result.html",a50= per[0][0], a75=per[0][1],a95 = per[0][2],iph50 = per[1][0], iph75 = per[1][1], iph95 = per[1][2],ipa50 = per[2][0], ipa75 = per[2][1], ipa95 = per[2][2],mac50 = per[3][0], mac75 = per[3][1], mac95 = per[3][2])

	return render_template("hello.html")


if __name__ == '__main__':
    # Bind to PORT if defined, otherwise default to 5000.
    port = int(os.environ.get('PORT', 5000))
    app.run(host='0.0.0.0', port=port)
